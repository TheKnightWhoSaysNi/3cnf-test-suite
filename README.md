# 3-SAT Solver Test Suite

An extensive test suite designed to test the solving capabilities of a 3-CNF expression solver.
The input for the expression solve must consist of 0 or more 3-CNF terms in `( a + b + c )` form. Variables used range from `a-z` and a not is indicated by `'`

The solver must be designed so that it's output is one of three:

1. If the given expression is satisfiable then the output must be
    ```
     satisfiable by [list]
     ```
     where list is a list of literal values which satisfy the expression.
     Ex:
     ```
     satisfiable by [a=true, b=false]
     ```
     An empty list is an acceptable output

2. If the given expression is unsatisfiable then the output must be:
	  ```
	  unsatisfiable
      ```

3. If there is an error in the input an error message saying so is expected. As long as the word `error` is in the output the test will pass

If a test is satisfiable the scrip will check if the provided expression is satisfiable with the literal values returned by the solver. The script can't check if a provided test is unsatisfiable, this must be known before hand.

## Usage

### Build
Unzip all the contents into any desired directory
The script is a bash script. All that is needed is to allow execution of the file:
```
$ chmod +x runTests
```
### Run
Usage:
```
$ cd 3cnf-test-suite
$ ./runTests
```
The script looks for an executable file called `solver` in the current directory and runs a small number of tests on it. Using various flags it can be customized:
```
	-f FILENAME	Executable to run. Default "solver"
				This is the path to the executable file
	-l LEVEL	Level of test:
				0 -> 	9 satisfiable tests
						3 unsatisfiable tests
						18 erroneous tests
				1 -> 	20 satisfiable tests
						5 unsatisfiable tests
						18 erroneous tests
				2 -> 	40 satisfiable tests
						10 unsatisfiable tests
						18 erroneous tests
				3 -> 	50 satisfiable tests
						15 unsatisfiable tests
						18 erroneous tests

	Optionally each type of test can be customized:

	-s AMOUNT	Number of satisfiable test to run. Max 1009
	-u AMOUNT	Number of unsatisfiable test to run. Max 1003
	-e AMOUNT	Number of erroneous test to run. Max 18
```

## Notes

This test script comes with over 2000 tests. If you would like to add your own tests simply place them in the TEST_DIR. Test files must end in one of 3 ways to indicate their expected result:

1. *sat - A satisfiable test
2. *uns - An unsatisfiable test
3. *err - An error in input test

Note that for the most part unsatisfiable tests will take a long time to compute. Beware of that.

## Sources

* https://www.cs.ubc.ca/~hoos/SATLIB/benchm.html
* Marc Schroeder
